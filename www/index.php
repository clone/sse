<?php
error_reporting(E_ALL);
require_once('_main.inc.php');


//////////////////////////////////////////////////////////////////////////
// parse arguments

// text: search field
$q = (isset($_REQUEST['q']) ? $_REQUEST['q'] : '');

// checkbox: subword
$subword = ((isset($_REQUEST['subword']) and $_REQUEST['subword']) 
	    ? 'checked' : false);

// checkbox: casesensitiv
$case = ((isset($_REQUEST['case']) and $_REQUEST['case']) 
	 ? 'checked' : false);
$case = false;

// results per page
$num = $num_default;
if (isset($_REQUEST['num'])) {
  $t = intval($_REQUEST['num']);
  if ($t > 100) $t = 100;
  if ($t > 0) $num = $t;
}

// start with result XXX
$start = 0;
if (isset($_REQUEST['start'])) {
  $t = intval($_REQUEST['start']);
  if ($t > 0) $start = $t;
}


//////////////////////////////////////////////////////////////////////////
// page header

$title = 'Source Search Engine'.($q ? ' - '.htmlentities($q) : '');

header('Content-Type: application/xhtml+xml');

echo <<<ECHOFORM
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<?xml-stylesheet type="text/xsl" href="style.xsl"?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>$title</title>
</head>
<body>
<p><b>Better get an XSLT capable browser!</b></p>
ECHOFORM;

db_connect();


$stats = array();
$res = mysql_query("SHOW TABLE STATUS");
if ($res) {
  while ($r = mysql_fetch_array($res)) {
    $stats[$r['Name']] = $r['Rows'];
  }
}

foreach ($stats as $table => $n) {
  echo('<p style="display:none;"><span id="n_'.htmlentities(strtolower($table)).'">'.intval($n)."</span> in ".htmlentities($table)."</p>\n");
}



//////////////////////////////////////////////////////////////////////////
// create form fields

$ME = $_SERVER['PHP_SELF'];
$escaped_q = htmlentities($q);
$subword_checked = ($subword ? ' checked="checked" ' : '');
$case_checked = ($case ? ' checked="checked" ' : '');
$hidden_form = ($num != $num_default ? '<input type="hidden" name="num" value="'.$num.'" />' : '');

// prettiness hack
$ME = preg_replace('/index\.php$/', '', $ME);

echo <<<ECHOFORM

<div class="formbox">
<form action="$ME" method="get">$hidden_form
<p><input type="text" name="q" value="$escaped_q" /> <input type="submit" value="Search" /></p>
<p><input type="checkbox" id="subword" name="subword" value="1" $subword_checked /> <label for="subword">subwords</label></p>
</form>
</div>

ECHOFORM;



//////////////////////////////////////////////////////////////////////////
// create sql query

$sql = '';
while ($q) {

  $s = split_q($q);

  if (!count($s)) {
    break;
  }

  if (count($s) == 1) {
    $where = ($subword 
	      ? " w.text LIKE '".db_escape($s[0])."%' " 
	      : " w.text='".db_escape($s[0])."' AND w.type='word' "
	      );
  } else {
    $where_parts = array();
    foreach ($s as $e) {
      $where_parts[] = ($subword 
			? "w.text LIKE '".db_escape($e)."%'"
			: "w.text='".db_escape($e)."'");
    }

    $where = " (".implode(' OR ', $where_parts).") ".
      ($subword ? '' : " AND w.type='word' ");
  }


  $sql = "SELECT COUNT(*)/COUNT(DISTINCT pr.id) AS keywords_cnt, ".
    " SUM(w.cnt)/COUNT(DISTINCT pr.id) AS cnt, " .
    " f.path, f.language_id, pr.id AS prid, pr.name AS package_name ".
    " FROM word AS w, file AS f, package AS p, provider_record AS pr ".
    " WHERE p.id=pr.package_id AND ".$where.
    " AND w.file_id=f.id AND f.package_id=p.id ".
    " AND f.crawler_id=w.crawler_id AND f.crawler_id=p.crawler_id AND pr.crawler_id=f.crawler_id ".
    " GROUP BY f.crawler_id, f.id ".
    " ORDER BY keywords_cnt DESC, w.cnt DESC ".
    " LIMIT ".$start.", ".$num;

  //echo(htmlentities($sql)); exit;
  break;
}




//////////////////////////////////////////////////////////////////////////
// query DB

$total = 0;
while (isset($sql) and $sql) {

  $res = mysql_query($sql);
  if ($res === false) {
    echo('<p class="error">'.mysql_errno().': '.mysql_error()."<br />\n".$sql."</p>\n");
    break;
  }

  $res2 = mysql_query("SELECT COUNT(*) AS cnt FROM word AS w WHERE ".$where);
  if ($r2 = mysql_fetch_array($res2)) {
    $total = $r2['cnt'];
  }
  //$total = mysql_num_rows($res);
  break;
}


$max_page = max(1, ceil($total/$num));
$max_start = ($max_page-1)*$num;

// behind last result page
//if ($start > $max_start) $start = $max_start;

// start not aligned
//$start = floor($start/$num)*$num;





//////////////////////////////////////////////////////////////////////////
// show results

if ($total) {

  echo('<p>Results: ca. <span id="results_total">'.$total."</span></p>\n".
       '<ul class="results" id="result_list">'."\n");

  while ($r = mysql_fetch_array($res)) {
    $r['keywords_cnt'] = intval($r['keywords_cnt']);
    if ($r['keywords_cnt'] < 2) $r['keywords_cnt'] = '';
    echo('<li><span class="package_name">'.htmlentities($r['package_name']).'</span>: '.
	 '<a class="file_link" href="show/'.htmlentities($r['prid']).'/'.htmlentities($r['path']).'?q='.htmlentities($q).'#first">'.
	 htmlentities($r['path']).'</a> '.
	 '(<span class="hits">'.intval($r['cnt']).'</span> hits'.
	 ($r['keywords_cnt'] ? ' - <span class="words">'.$r['keywords_cnt'].'</span> different words' : '').
	 ')'."</li>\n");
  }

  echo("</ul>\n");
}

//////////////////////////////////////////////////////////////////////////
// links
if ($max_page > 1) {

  $l = $ME.'?q='.urlencode($q).
    ($case ? '&amp;case=1' : '').
    ($subword ? '&amp;subword=1' : '').
    ($num != $num_default ? '&amp;num='.$num : '').
    '&amp;start=';

  echo('<ul class="links">'."\n".
       ($start ? '<li><a id="browse_first" href="'.$l.'0">first</a>'."</li>\n" : '').
       ($start ? '<li><a id="browse_previous" href="'.$l.max(0, $start-$num).'">previous</a>'."</li>\n" : ''));

  $lnkcnt = 10;
  for ($i = max(0,$start-($lnkcnt*$num)); $i <= min($max_start, $start+($lnkcnt*$num)); $i+=$num) {
    echo('<li><a class="browse_page" href="'.$l.$i.'"'.($i == $start ? ' id="browse_page_active" ' : '').'>'.(($i/$num)+1).'</a>'."</li>\n");
  }

  echo(($start < $max_start ? '<li><a id="browse_next" href="'.$l.min($max_start, $start+$num).'">next</a>'."</li>\n" : '').
       //($start < $max_start ? '<li><a id="browse_last" href="'.$l.$max_start.'">last</a>'."</li>\n" : '').
       "</ul>\n");

}




echo("</body>\n</html>");
?>
