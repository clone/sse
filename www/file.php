<?php
error_reporting(E_ALL);
require_once('_main.inc.php');

$format = 'show';
$prid = '';
$f_path = '';
if (preg_match('/\/(show|txt)\/([^\:]+\:[^\/]+)\/([^\?]+)/', $_SERVER['REQUEST_URI'], $mat)) {
  $format = $mat[1];
  $prid = $mat[2];
  $f_path = $mat[3];
}

db_connect();
$query = "SELECT p.path AS ppath, f.path AS fpath ".
" FROM provider_record AS pr, package AS p, file AS f ".
" WHERE pr.id='".db_escape($prid)."' AND pr.package_id=p.id AND pr.crawler_id=p.crawler_id AND ".
" f.crawler_id=p.crawler_id AND f.package_id=p.id AND f.path='".db_escape($f_path)."'";

$res = mysql_query($query);
if ($res === false) {
  die(mysql_error().'<br />'.$sql.mysql_errno());
}
if (!($r = mysql_fetch_array($res))) {
  header('Status: 404 Not Found');
  echo('404 Not found!');
  exit;
}



$file = sprintf($r['ppath'], $r['fpath']);


//////////////////////////////////////////////////////////////////////////
// txt

if ($format == 'txt') {
  header('Content-type: text/plain');

  $fh = @fopen($file, 'r');
  if (!$fh) die('failed to open file');
  
  while (!feof($fh)) {
    $n = rtrim(fgets($fh, 4096));
    $n = strtr($n, "\x1\x2\x3\x4\x5\x6\x7\x8\xa\xb\xc\xd\xe\xf\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f", 
	     '                               ');
    echo($n."\n");
  }
  @fclose($fh);

  exit;
}




//////////////////////////////////////////////////////////////////////////
// show
echo <<<PRINTHEADER
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>Source Search Engine</title>
<style type="text/css">
ol {font-size:12px;margin:0;padding:0;}
li {margin:0 0 0 50px;padding:0;}
pre {font-size:12px;padding:0;margin:0;}
.found0 {background-color:#ffff00;font-weight:bold;}
.found1 {background-color:#ff66ff;font-weight:bold;}
.found2 {background-color:#ff9966;font-weight:bold;}
.found3 {background-color:#00ff00;font-weight:bold;}
.found4 {background-color:#00ffff;font-weight:bold;}
</style>
</head>
<body>
PRINTHEADER;

$q = (isset($_REQUEST['q']) ? $_REQUEST['q'] : '');
$s = split_q($q);
$pattern = array();
$replacement = array();
$i = 0;
foreach ($s as $v) {
  $pattern[] = '/('.$v.')/i';
  $replacement[] = '<span class="found'.($i%5).'">$1</span>';
  $i++;
}

$fh = @fopen($file, 'r');
if (!$fh) die('failed to open file');

$found = false;
$i = 0;
echo('<ol>');
while (!feof($fh)) {
  echo('<li>');
  $n = fgets($fh, 4096);
  $n = strtr($n, "\x1\x2\x3\x4\x5\x6\x7\x8\xa\xb\xc\xd\xe\xf\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f", 
	     '                               ');
  $n = htmlentities($n);
  $o = preg_replace($pattern, $replacement, $n);
  if (!$found and $o != $n) {
    $found = true;
    echo('<a id="first"></a>');
  }
  $i++;
  //printf("<a name=\"%d\" id=\"%d\">%4d:</a> %s\n", $i, $i, $i, rtrim($o));
  $code = rtrim($o);
  if (!$code) $code = '&nbsp;';
  printf("<a id=\"l%d\"></a><pre>%s</pre></li>\n", $i, $code);
  
}
echo('</ol>');
fclose($fh);

echo <<<PRINTFOOTER
</body>
</html>
PRINTFOOTER;

?>