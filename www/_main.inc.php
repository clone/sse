<?php

function db_escape($s) { return mysql_escape_string($s); }

function db_connect() {
  mysql_pconnect('localhost', 'sse_ro', 'Shu5ahdo');
  mysql_select_db('sse');
}

function split_q($q) {
  $q = preg_replace('/[^a-zA-Z0-9_\s]/', '', trim($q));
  return preg_split('/\s/', $q, -1, PREG_SPLIT_NO_EMPTY);
}

function stripslashes_deep($value) {
  if (!is_array($value)) return stripslashes($value);
  $n = array();
  foreach ($value as $k => $v) {
    $n[stripslashes($k)] = stripslashes_deep($v);
  }
  return $n;
}

if (get_magic_quotes_gpc()) {
  $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
  $_POST = array_map('stripslashes_deep', $_POST);
  $_GET = array_map('stripslashes_deep', $_GET);
  $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
}

$num_default = 25;

?>