<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" version="1.0" encoding="iso-8859-15" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" indent="yes"/>

  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <link rel="stylesheet" type="text/css" href="style.css"/>
      </head>
       <body>

         <h1>Source Search Engine</h1>

         <xsl:copy-of select="//html:div[@class=&quot;formbox&quot;]"/>

         <xsl:if test="number(id(&quot;results_total&quot;)) = 0">
           <div>Your search yielded no results.</div>
         </xsl:if>

         <xsl:if test="number(id(&quot;results_total&quot;)) > 0">

           <div class="n_hits">ca. <xsl:value-of select="id(&quot;results_total&quot;)"/> hits.</div>

           <xsl:for-each select="id(&quot;result_list&quot;)/html:li">
             <div class="hit">
               <div class="hit_package">Project: <span class="name"><xsl:value-of select="html:span[@class=&quot;package_name&quot;]"/></span></div>               
               <div class="hit_file">File: <a class="file">
               <xsl:attribute name="href"><xsl:value-of select="html:a[@class=&quot;file_link&quot;]/@href"/></xsl:attribute> <xsl:value-of select="html:a[@class=&quot;file_link&quot;]"/></a></div>
               <div class="hit_statistics"><xsl:value-of select="html:span[@class=&quot;hits&quot;]"/> hits in <xsl:value-of select="html:span[@class=&quot;words&quot;]"/> different words.</div> 
             </div>
           </xsl:for-each>
         </xsl:if>
         
         <div class="linkbox">
         <xsl:for-each select="//html:ul[@class=&quot;links&quot;]">
           <xsl:copy-of select="."/>
         </xsl:for-each>
         </div>

         <div class="statistics">
           <xsl:value-of select="id(&quot;n_word&quot;) "/> words, 
           <xsl:value-of select="id(&quot;n_file&quot;) "/> files,
           <xsl:value-of select="id(&quot;n_package&quot;) "/> packages in database.
         </div>

         <div class="authors"><xsl:text>Brought to you by Lennart Poettering, Stephan P�hlsen and Sebastien Estienne, 2005</xsl:text></div>
       </body>
     </html>
  </xsl:template>

</xsl:stylesheet>
