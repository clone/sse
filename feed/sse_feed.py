#!/usr/bin/python

import sys, os, stat, string
from popen2 import Popen3

import sse_db
from sse_config import *
from sse_defs import *

supported_languages = [ {
    "extensions" : [".c", ".h", ".cc", ".hh", ".cpp", ".hpp"],
    "lexer" : SSE_DIR+"/sse_lex_c",
    "language_id" : SSE_LANGUAGE_C
    }]

def find_language(fn):
    
    for l in supported_languages:
        for e in l["extensions"]:
            if fn.lower().endswith(e):
                return l

    return None

def supported_source(fn):
    return not find_language(fn) is None

def process_source(archive, root, path, package_id, meta):
    print "(%s) Processing %s" % (archive, path)

    language = find_language(path)
    assert not language is None

    file_id = sse_db.new_file(package_id, path, language["language_id"])

    table = {}

    p = Popen3("%s %s" % (language["lexer"], os.path.join(root, path)))

    for identifier in p.fromchild:
    	t = identifier.strip()
        k = t.lower()[:40]

        try:
            table[k][1] += 1
        except KeyError:
            table[k] = [t, 1]

    for k, v in table.items():
        if v[0].startswith("S:"):
            sse_db.new_word(file_id, v[0][2:], True, v[1])
        else:
            sse_db.new_word(file_id, v[0], False, v[1])

    del table

    if p.wait() != 0:
        print "WARNING: Subprocess failed!"

    del p
