#!/usr/bin/python
import os, errno, time

def try_lock_file(fn):

    lp = "%s.%i" % (fn, os.getpid())
    fd = os.open(lp, os.O_RDWR|os.O_CREAT, 0666)

    try:
        os.link(lp, fn)
    
    except OSError, e:
    	os.close(fd)
    	os.unlink(lp)

	if e.errno != errno.EEXIST:
            raise e

        return False
	
    os.close(fd)
    os.unlink(lp)

    return True

def unlock_file(fn):

    os.unlink(fn)


if __name__ == "__main__":
    
    if not try_lock_file("test.lock"):
        print "FAILED"
    else:
        try:
            print "LOCKED"
            time.sleep(60)
        finally:
            unlock_file("test.lock")
            
        print "UNLOCKED"
    

    
    
        
