import os

HOME = os.environ["HOME"]

SSE_DIR = "/path/to/sse"
SSE_CRAWLER_ID = 0

SSE_DB_HOST = "localhost"
SSE_DB_USER= "sse_web"
SSE_DB_PASSWORD = "xxx"
SSE_DB_DATABASE = "sse"

#SSE_DEBIAN_URL_DOWNLOAD = "http://update.alturo-server.de/debian/"
#SSE_DEBIAN_URL_DOWNLOAD = "http://ftp.fr.debian.org/debian/"
SSE_DEBIAN_URL_DOWNLOAD = "http://ftp.us.debian.org/debian/"
SSE_DEBIAN_URL = "http://ftp.us.debian.org/debian/"

SSE_DISTRIBUTION = "unstable"
SSE_RELEASE = "main"

from sse_config_local import *
    
assert SSE_CRAWLER_ID != 0
