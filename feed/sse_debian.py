#!/usr/bin/python

import sys

from sse_defs import *
from sse_config import *
import sse_grab

def process_entry(entry):

    for k, v in entry.items():
        entry[k] = v.strip()

    meta = {}

    for l in entry["Files"].split("\n"):

        a = l.split()[2]

        if a.endswith(".tar.gz"):
            meta["archive"] = a
            break

    meta["name"] = entry["Package"]
    meta["version"] = entry["Version"]
    meta["archive-url"] = SSE_DEBIAN_URL + entry["Directory"] + "/" + meta["archive"]
    meta["archive-url-download"] = SSE_DEBIAN_URL_DOWNLOAD + entry["Directory"] + "/" + meta["archive"]
    meta["license"] = "DFSG approved"
    meta["project-url"] = "http://packages.debian.org/"+ SSE_DISTRIBUTION + "/source/" + entry["Package"]

    print "Next record '%s' (debian:%s)" % (meta["name"], meta["name"])

    sse_grab.grab_archive(meta, "debian:" + meta["name"], SSE_PROVIDER_DEBIAN)

def process_list(f):

    while True:

        entry = {}
        key = None

        while True:

            ln = f.readline().strip("\n\r")

            if ln == "":
                break

            elif ln[0].isspace():
                entry[key] += "\n" + ln[1:]

            else:
                key, data = ln.split(":", 1)
                entry[key] = data

        process_entry(entry)


if __name__ == "__main__":
    process_list(sys.stdin)
