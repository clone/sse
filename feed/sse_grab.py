#!/usr/bin/python

import sys, urllib2, os, socket, httplib

import sse_tar, sse_lock
from sse_defs import *
from sse_config import *

socket.setdefaulttimeout(SSE_SOCKET_TIMEOUT)

def grab_archive(meta, recid, provider_id = SSE_PROVIDER_NONE):
    
    try:
        os.mkdir("%s/download" % HOME)
    except:
        pass
    
    fn = os.path.join(HOME, "download", recid)
    lock_fn = fn + ".lock"

    if not sse_lock.try_lock_file(lock_fn):
        print "Skipping locked archive '%s'." % recid
        return

    try:
        download = False

        try:
            f = open(fn+".release", "r")
        except IOError:
            download = True
        else:
            download = f.read(SSE_BLOCK_SIZE) != meta["version"]
            f.close()

        try:
            archive_url = meta["archive-url-download"]
        except KeyError:
            archive_url = meta["archive-url"]

        if not download:
            print "File %s up-to-date." % archive_url
            return

        print "Downloading %s..." % archive_url

        dst = None
        src = None
        
        try:
            dst = file(fn, "w")
            src = urllib2.urlopen(archive_url)
            m = 0

            while True:

                if m > SSE_MAX_DOWNLOAD:
                    os.unlink(fn)
                    print "WARNING: Archive too large, ignoring."
                    return

                data = src.read(SSE_BLOCK_SIZE)

                if len(data) <= 0:
                    break
        
                dst.write(data)
                m += len(data)

            dst.close()
            del src

        except KeyboardInterrupt, e:
            raise e
        
        except Exception, e:
            os.unlink(fn)
            print "WARNING: Failed to download %s: %s!" % (archive_url, e)
            return

        sse_tar.process_archive(fn, meta, recid, provider_id)
        os.unlink(fn)

        try:
            f = open(fn+".release", "w")
        except:
            os.unlink(fn)
        
        f.write(meta["version"])
        f.close()

    finally:
        sse_lock.unlock_file(lock_fn)
                            
