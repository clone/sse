/* --*-c-*-- $Id$ */

%{

#include <stdio.h>
#include <ctype.h>

static void print_with_subwords(const char *w) {
    const char *e;

    printf("%s\n", w);

    if ((e = strchr(w, '_'))) {

        /* Split at dashes.*/

        do {
            for (; *e == '_'; e++);

            if (strlen(e) < 3)
                break;
            
            printf("S:%s\n", e);

            e = strchr(e, '_');
        } while (e);

    } else {

        /* Split a place where an uppercase letter follows a lowercase
         * letter */

        e = w;
        for (;;) {
            for (; *e && !(islower(*e) && isupper(*(e+1))); e++);

            if (!*e)
                break;
            
            e++;
            
            if (strlen(e) < 3)
                break;

            printf("S:%s\n", e);
        }
    }
        
}
    
%}

%Start DEF CCOMMENT CPPCOMMENT STRING STRINGESC CHAR CHARESC PREPROC

IDCHAR [_a-zA-Z0-9]
NIDCHAR [^_a-zA-Z0-9]

%%

 BEGIN DEF;

<DEF>^#ifn?def |
<DEF>^#if      |
<DEF>^#define  |
<DEF>^#undef   ;

<DEF>^#. { BEGIN CPPCOMMENT; }

<DEF>"/*" { BEGIN CCOMMENT; }
<CCOMMENT>"*/" { BEGIN DEF; }
<CCOMMENT>\n |
<CCOMMENT>. ;

<DEF>"//" { BEGIN CPPCOMMENT; }
<CPPCOMMENT>\n { BEGIN DEF; }
<CPPCOMMENT>. ;

<DEF>"\"" { BEGIN STRING; }
<STRING>"\\" { BEGIN STRINGESC; }
<STRING>"\"" { BEGIN DEF; }
<STRING>. |
<STRING>\n ;
<STRINGESC>. { BEGIN STRING; }

<DEF>"'" { BEGIN CHAR; }
<CHAR>"\\" { BEGIN CHARESC; }
<CHAR>"'" { BEGIN DEF; }
<CHAR>\n |
<CHAR>. ;
<CHARESC>. { BEGIN CHAR; }

<DEF>[a-zA-Z_][a-zA-Z_0-9]{3,} { print_with_subwords(yytext); }

<DEF>[0-9]+ ;
<DEF>0x[0-9a-fA-F]+ ;

<DEF>"\n" |
<DEF>. ;

%%


int main(int argc, char *argv[]) {

    if (argc <= 1)
        yylex();
    else {
        int i;
        
        for (i = 1; i < argc; i++) {
            if (!(freopen(argv[i], "r", stdin))) {
                fprintf(stderr, "Failed to open file: %s\n", strerror(errno));
                return 1;
            }
            yylex();
        }
    }

    return 0;
}
